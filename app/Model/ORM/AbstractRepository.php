<?php declare(strict_types = 1);

namespace App\Model\ORM;

use Nextras\Orm\Repository\Repository;

abstract class AbstractRepository extends Repository
{

}
