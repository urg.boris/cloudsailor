<?php declare(strict_types = 1);

namespace App\Model\ORM;

use App\Model\ORM\Product\ProductRepository;
use Nextras\Orm\Model\Model;

/**
 * @property-read ProductRepository $product
 */
class Orm extends Model
{

}
