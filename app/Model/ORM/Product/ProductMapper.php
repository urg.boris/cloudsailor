<?php declare(strict_types = 1);

namespace App\Model\ORM\Product;

use App\Model\ORM\AbstractMapper;

class ProductMapper extends AbstractMapper
{

    public function getTableName(): string
    {
        return 'product';
    }

}
