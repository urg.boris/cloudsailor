<?php declare(strict_types = 1);

namespace App\Model\ORM\Product;

use App\Model\ORM\AbstractEntity;
use Nextras\Dbal\Utils\DateTimeImmutable;
use Nextras\Orm\Entity\ToArrayConverter;

/**
 * @property int $id {primary}
 * @property string $name
 * @property float $price
 * @property DateTimeImmutable $createdAt {default now}
 * @property DateTimeImmutable|NULL $updatedAt
 */
class Product extends AbstractEntity
{

    public function toArray(int $mode = ToArrayConverter::RELATIONSHIP_AS_IS): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'price' => $this->price,
            'createdAt' => $this->createdAt->format('Y-m-d H:i:s'),
            'updatedAt' => $this->updatedAt?->format('Y-m-d H:i:s'),
        ];
    }

}
