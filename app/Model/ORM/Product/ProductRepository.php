<?php declare(strict_types = 1);

namespace App\Model\ORM\Product;

use App\Model\ORM\AbstractRepository;

class ProductRepository extends AbstractRepository
{

    public static function getEntityClassNames(): array
    {
        return [Product::class];
    }

}
