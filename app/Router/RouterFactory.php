<?php declare(strict_types = 1);

namespace App\Router;

use Apitte\Presenter\ApiRoute;
use Nette;
use Nette\Application\Routers\RouteList;

final class RouterFactory
{

    use Nette\StaticClass;

    public static function createRouter(): RouteList
    {
        $router = new RouteList();
        $router[] = new ApiRoute('api/');
        $router->addRoute('<presenter>[/<action>][/<id>]', 'Homepage:default');

        return $router;
    }

}
