<?php declare(strict_types = 1);

namespace App\Api\V1\Schemas;

use Swaggest\JsonSchema\Constraint\Properties;
use Swaggest\JsonSchema\Schema;
use Swaggest\JsonSchema\Structure\ClassStructure;

class PostSchema extends ClassStructure
{

    public Schema $name;

    public Schema $price;

    /**
     * @param Properties|static $properties
     * @param Schema $ownerSchema
     */
    public static function setUpProperties($properties, Schema $ownerSchema): void
    {
        $properties->name = Schema::string(); // @phpstan-ignore-line
        $properties->price = Schema::number(); // @phpstan-ignore-line
    }

}
