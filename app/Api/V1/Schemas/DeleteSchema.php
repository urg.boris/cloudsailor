<?php declare(strict_types = 1);

namespace App\Api\V1\Schemas;

use Swaggest\JsonSchema\Constraint\Properties;
use Swaggest\JsonSchema\Schema;
use Swaggest\JsonSchema\Structure\ClassStructure;

class DeleteSchema extends ClassStructure
{

    public Schema $id;

    /**
     * @param Properties|static $properties
     * @param Schema $ownerSchema
     */
    public static function setUpProperties($properties, Schema $ownerSchema): void
    {
        $properties->id = Schema::integer(); // @phpstan-ignore-line

        $ownerSchema->required = [self::names()->id]; // @phpstan-ignore-line
    }

}
