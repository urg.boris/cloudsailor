<?php declare(strict_types = 1);

namespace App\Api\V1\Controllers;

use Apitte\Core\Http\ApiRequest;
use Apitte\Core\Http\ApiResponse;
use App\Api\V1\Schemas\DeleteSchema;
use App\Api\V1\Schemas\GetSchema;
use App\Api\V1\Schemas\PutSchema;
use App\Model\ORM\Product\Product;
use App\Model\ORM\Product\ProductRepository;
use Exception;
use Throwable;

/**
 * @Apitte\Core\Annotation\Controller\Path("/product")
 */
class ProductController extends BaseV1Controller
{

    public function __construct(
        private ProductRepository $productRepository,
    )
    {
    }

    /**
     * @Apitte\Core\Annotation\Controller\Path("/")
     * @Apitte\Core\Annotation\Controller\Method("POST")
     */
    public function post(ApiRequest $request, ApiResponse $response): ApiResponse
    {
        try {

            $jsonBody = $request->getJsonBody();

            $jsonData = (array) GetSchema::import($jsonBody);

            $product = new Product();
            $product->name = (array_key_exists('name', $jsonData) ? (string) $jsonData['name'] : '');
            $product->price = (array_key_exists('price', $jsonData) ? (float) $jsonData['price'] : 0);

            $this->productRepository->persistAndFlush($product);

            return $response->writeJsonBody($product->toArray());
        } catch (Throwable $e) {
            return $this->getErrorResponse($response, $e->getMessage());
        }
    }

    /**
     * @Apitte\Core\Annotation\Controller\Path("/")
     * @Apitte\Core\Annotation\Controller\Method("GET")
     */
    public function get(ApiRequest $request, ApiResponse $response): ApiResponse
    {
        try {
            $jsonBody = $request->getJsonBody();

            $jsonData = (array) GetSchema::import($jsonBody);

            $getSingleProduct = array_key_exists('id', $jsonData);

            if ($getSingleProduct) {
                assert(is_int($jsonData['id']));
                return $this->getSingleProductResponse($jsonData['id'], $response);

            }

            return $this->getAllProductsResponse($response);
        } catch (Throwable $e) {
            return $this->getErrorResponse($response, $e->getMessage());
        }
    }

    /**
     * @Apitte\Core\Annotation\Controller\Path("/")
     * @Apitte\Core\Annotation\Controller\Method("DELETE")
     */
    public function delete(ApiRequest $request, ApiResponse $response): ApiResponse
    {
        try {
            $jsonBody = $request->getJsonBody();

            $jsonData = (array) DeleteSchema::import($jsonBody);

            assert(is_int($jsonData['id']));

            $product = $this->getProductById($jsonData['id']);

            if (!$product) {
                throw new Exception('entity not in db');
            }

            $this->productRepository->removeAndFlush($product);
        } catch (Throwable $e) {
            $this->getErrorResponse($response, 'product not found');
            return $response;
        }

        return $this->getSuccessResponse($response);
    }

    /**
     * @Apitte\Core\Annotation\Controller\Path("/")
     * @Apitte\Core\Annotation\Controller\Method("PUT")
     */
    public function put(ApiRequest $request, ApiResponse $response): ApiResponse
    {
        try {
            $jsonBody = $request->getJsonBody();

            $jsonData = (array) PutSchema::import($jsonBody);

            assert(is_int($jsonData['id']));

            $product = $this->getProductById($jsonData['id']);

            if (!$product) {
                throw new Exception('entity not in db');
            }

            if (array_key_exists('name', $jsonData)) {
                $product->name = (string) $jsonData['name'];
            }

            if (array_key_exists('price', $jsonData)) {
                $product->price = (float) $jsonData['price'];
            }

            $this->productRepository->persistAndFlush($product);
        } catch (Throwable $e) {
            $this->getErrorResponse($response, 'product not found');
            return $response;
        }

        return $response->writeJsonBody($product->toArray());
    }


    private function getAllProductsResponse(ApiResponse $response): ApiResponse
    {
        try {
            /**
             * @var Product[] $entities
             */
            $entities = $this->productRepository->findAll()->fetchAll();

            $data = array_map(fn($value): array => $value->toArray(), $entities);

            return $response->writeJsonBody($data);
        } catch (Throwable $e) {
            $this->getErrorResponse($response, 'products not found');
            return $response;
        }
    }


    private function getSingleProductResponse(int $id, ApiResponse $response): ApiResponse
    {
        try {
            $entity = $this->getProductById($id);
        } catch (Throwable $e) {
            $this->getErrorResponse($response, sprintf('product not found - %s', $e->getMessage()));
            return $response;
        }

        if (!$entity) {
            return $this->getErrorResponse($response, 'product not found');
        }

        return $response->writeJsonBody($entity->toArray());
    }

    private function getErrorResponse(ApiResponse $response, string $msg): ApiResponse
    {
        $response->withStatus(404);
        $response->writeJsonBody(['error' => $msg]);
        return $response;
    }

    private function getSuccessResponse(ApiResponse $response, string $msg = 'ok'): ApiResponse
    {
        $response->withStatus(200);
        $response->writeJsonBody(['response' => $msg]);
        return $response;
    }


    private function getProductById(int $id): Product|null
    {
        return $this->productRepository->getById($id);
    }

}
