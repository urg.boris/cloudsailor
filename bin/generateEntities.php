<?php

declare(strict_types=1);

require __DIR__ . '/../vendor/autoload.php';

use Contributte\Nextras\Orm\Generator\Analyser\Database\DatabaseAnalyser;
use Contributte\Nextras\Orm\Generator\Config\Impl\SeparateConfig;
use Contributte\Nextras\Orm\Generator\Config\Impl\TogetherConfig;
use Contributte\Nextras\Orm\Generator\SimpleFactory;

$config = [
    'output' => __DIR__ . '/../App/Model/NewORM',
    //other options
];
$factory = new SimpleFactory(
    new TogetherConfig($config),
    new DatabaseAnalyser('mysql:host=db;dbname=cloudsailor', 'root', 'root')
);

$factory->create()->generate();