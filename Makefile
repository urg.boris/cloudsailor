docker-exec-t=docker-compose exec -T app
docker-exec=docker-compose exec app

bash:
	${docker-exec} $(filter-out $@,$(MAKECMDGOALS))

composer:
	${docker-exec} composer $(filter-out $@,$(MAKECMDGOALS))

stan:
	${docker-exec-t} vendor/bin/phpstan analyse -l 9 app --ansi

csf:
	${docker-exec-t} vendor/bin/codefixer ./app

cs:
	${docker-exec-t} vendor/bin/codesniffer ./app

rmc:
	${docker-exec-t} rm -rf ./temp/cache