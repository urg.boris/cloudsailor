install

1. rename config/docker.local.neon to config/local.neon
2. docker-compose --up -d
3. make composer install

http://localhost:60000/api/v1/product

POST
	{
	  "name": "Korouhev",
	  "price": 7.5
	}

PUT
	{
	  "id": 1,
	  "name": "Korouhev DX-6000",
	  "price": 8.6
	}

GET 
	{} - pro vsechny

	{
	  "id": 1
	}
	
DELETE
	{
	  "id":1
	}

rozsireni
	zabezpeceni
		-ssl
		-povoleni url(pokud je pouze nekolik pristupovych mist) header("Access-Control-Allow-Origin: https://pristup.cz");
		-encode+decode json, napr. JWT
	dokumentace
		-pomoci OpenApi anotaci a nasledneho pouziti https://zircote.github.io/swagger-php/
	verzovani
		- pridani dalsich verzi do app/api a vytvoreni prislusnych kontroleru pro routovani (napr. v2 - BaseV2Controller)
	filtrace
		- pridani volitelnych parametru offset, limit pro strankovani
	testy
		-testovat Api controllery(napr. ProductController) - namockovat ApiRequest ->getJsonBody() a otestovat jak se chovaji metody get, post, put, delete
